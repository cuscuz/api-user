FROM openjdk:17-alpine

ARG JAR_FILE=target/*.jar
ARG NEW_RELIC_FILE=newrelic/newrelic.yml
ARG NEW_RELIC_JAR=newrelic/newrelic.jar

COPY ${NEW_RELIC_FILE} newrelic.yml
COPY ${NEW_RELIC_JAR} newrelic.jar
COPY /target/*.jar app.jar

ENTRYPOINT [ "java","-javaagent:/newrelic.jar","-jar","/app.jar"]
