package br.com.desafio2.ilab.usuarios;

import br.com.desafio2.ilab.usuarios.models.User;
import br.com.desafio2.ilab.usuarios.repositories.UserRepository;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
public class UserIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Test
    @DisplayName("Deve consultar todos os usuários e retornar uma lista paginada")
    void listUsers() throws Exception {
        int page = 0;
        int limit = 5;
        int size = this.userRepository.findAll().size();
        System.out.println(size);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/users?page={page}&limit={limit}", page, limit)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.currentPage").value(page))
                .andExpect(MockMvcResultMatchers.jsonPath("$.limit").value(limit));

    }

    @Test
    @DisplayName("Deve consultar um usuário pelo ID e retornar o objeto User")
    @Sql(statements = "INSERT INTO users (name, email, cpf) VALUES ('Joao', 'joao@email.com', '50998637017')")
    void getUser() throws Exception {
        User findUser = this.userRepository.findByEmail("joao@email.com").get();

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", findUser.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Joao"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cpf").value("50998637017"));

    }

    @Test
    @DisplayName("Deve salvar usuário no banco e retornar o objeto User")
    void saveUser() throws Exception {

        JSONObject requestBody = new JSONObject();
        requestBody.put("name", "Jonas");
        requestBody.put("email", "jonas@email.com");
        requestBody.put("cpf", "666.862.660-27");

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody.toString())
                        .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    @DisplayName("Deve atualizar os dados do usuário e retornar o objeto do User atualizado")
    void updateUser() throws Exception {
        User user = this.userRepository.findById(1).get();

        JSONObject requestBody = new JSONObject();
        requestBody.put("name", "Lucas Kennedy");
        requestBody.put("email", user.getEmail());
        requestBody.put("cpf", user.getCpf());

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.put("/users/{id}", user.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody.toString())
                        .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucas Kennedy"));

    }

}
