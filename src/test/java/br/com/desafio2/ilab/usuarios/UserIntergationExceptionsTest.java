package br.com.desafio2.ilab.usuarios;

import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.desafio2.ilab.usuarios.providers.OrderApiProvider;
import br.com.desafio2.ilab.usuarios.repositories.UserRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
public class UserIntergationExceptionsTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrderApiProvider orderApiProvider;

    @Test
    @Sql(statements = "DELETE FROM users WHERE id = 999")
    @DisplayName("Deve consultar usuário que não existe e lançar exception com status 404 NOT_FOUND")
    void getUserNotFound() throws Exception {
        int userId = 999;

        ResultActions result = mockMvc
                .perform(MockMvcRequestBuilders.get("/users/{id}", userId).accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    @DisplayName("Deve consultar usuário com id de tipo inválido e lançar exception com status 400 BAD_REQUEST")
    void getUserByIdInvalid() throws Exception {
        String userId = "s";

        ResultActions result = mockMvc
                .perform(MockMvcRequestBuilders.get("/users/{id}", userId).accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(this.userRepository, Mockito.never()).findById(Mockito.anyInt());
    }

    @Test
    @DisplayName("Deve tentar cadastrar com email que já existe e lançar exception com status 400 BAD_REQUEST")
    @Sql(statements = "INSERT INTO users (name, email, cpf) values ('Luzia Malu Aparício', 'luzia.malu@fabiooliva.com.br','00309376394')")
    void saveUserExistingEmail() throws Exception {
        JSONObject requestBody = new JSONObject();
        requestBody.put("name", "Julia");
        requestBody.put("email", "Luzia Malu Aparício");
        requestBody.put("cpf", "223.753.803-47");

        ResultActions result = mockMvc
                .perform(MockMvcRequestBuilders.post("/users").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON).content(requestBody.toString()))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(this.userRepository, Mockito.never()).save(Mockito.any());

    }

    @Test
    @DisplayName("Deve tentar cadastrar com cpf que já existe e lançar exception com status 400 BAD_REQUEST")
    @Sql(statements = "INSERT INTO users (name, email, cpf) values ('Severino Yuri Isaac Duarte', 'severino.yuri@deze.com.br','77597176368')")
    void saveUserExistingCpf() throws Exception {
        JSONObject requestBody = new JSONObject();
        requestBody.put("name", "Yuri");
        requestBody.put("email", "yuri@deze.com.br");
        requestBody.put("cpf", "775.971.763-68");

        ResultActions result = mockMvc
                .perform(MockMvcRequestBuilders.post("/users").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON).content(requestBody.toString()))
                .andDo(MockMvcResultHandlers.print());

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(this.userRepository, Mockito.never()).save(Mockito.any());

    }

    // @Test
    // @DisplayName("Deve tentar deletar um usuário que não existe e então lançar
    // uma exception com status 404 NOT_FOUND")
    // void deleteuser() throws Exception {
    // int userId = 999;

    // ResultActions result =
    // mockMvc.perform(MockMvcRequestBuilders.delete("/users/{id}", userId))
    // .andDo(MockMvcResultHandlers.print());

    // result.andExpect(MockMvcResultMatchers.status().isBadRequest());

    // Mockito.verify(this.userRepository,
    // Mockito.never()).deleteById(Mockito.anyInt());
    // }
}
