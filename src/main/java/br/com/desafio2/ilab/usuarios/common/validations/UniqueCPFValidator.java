package br.com.desafio2.ilab.usuarios.common.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.desafio2.ilab.usuarios.models.User;
import br.com.desafio2.ilab.usuarios.repositories.UserRepository;

import java.util.Optional;

public class UniqueCPFValidator implements ConstraintValidator<UniqueCPF, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        value = value.replace(".", "");
        value = value.replace("-", "");

        Optional<User> inDB = userRepository.findByCpf(value);

        if (inDB.isEmpty()) {
            return true;
        }

        return false;
    }

}