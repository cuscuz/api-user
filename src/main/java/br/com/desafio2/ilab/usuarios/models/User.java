package br.com.desafio2.ilab.usuarios.models;

import br.com.desafio2.ilab.usuarios.common.validations.UniqueCPF;
import br.com.desafio2.ilab.usuarios.common.validations.UniqueEmail;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 50)
    @Size(max = 50, message = "Limite de 50 caracteres.")
    @NotBlank(message = "O campo nome é obrigatório")
    private String name;

    @Column(nullable = false, length = 11, unique = true)
    @NotBlank(message = "O campo cpf é obrigatório")
    @CPF(message = "CPF inválido")
    @UniqueCPF
    private String cpf;

    @Column(nullable = false, length = 100, unique = true)
    @NotBlank(message = "O campo email é obrigatório")
    @Email(message = "Email inválido")
    @UniqueEmail
    private String email;

    private String phone;

    private LocalDate birthdate; // format 2022-06-02

}
