package br.com.desafio2.ilab.usuarios.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio2.ilab.usuarios.DTO.UserPaginationDTO;
import br.com.desafio2.ilab.usuarios.DTO.UserUpdateDTO;
import br.com.desafio2.ilab.usuarios.common.errors.DefaultErrorResponse;
import br.com.desafio2.ilab.usuarios.models.User;
import br.com.desafio2.ilab.usuarios.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@SecurityScheme(name = "Token JWT", bearerFormat = "JWT", type = SecuritySchemeType.HTTP, scheme = "bearer")
@SecurityRequirement(name = "Token JWT", scopes = "write: read")
@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(summary = "Lista usuários")
    @ApiResponse(responseCode = "200", description = "Paginação de usuários.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserPaginationDTO.class)))
    @GetMapping
    public UserPaginationDTO list(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "limit", required = false, defaultValue = "20") Integer limit) {
        return userService.list(page, limit);
    }

    @Operation(summary = "Consulta usuário pelo ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuário encontrado.", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "404", description = "Usuário não encontrado.", content = @Content),
            @ApiResponse(responseCode = "400", description = "Id inválido.", content = @Content)
    })
    @GetMapping("/{id}")
    public User get(@PathVariable Integer id) {
        return userService.get(id);
    }

    @Operation(summary = "Cria um usuário")
    @ApiResponse(responseCode = "201", description = "Usuário criado.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class)))
    @ApiResponse(responseCode = "400", description = "Campos inválidos.", content = @Content(mediaType = "application/json", schema = @Schema(implementation = DefaultErrorResponse.class, hidden = true)))
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User save(@Valid @RequestBody User user) {
        return userService.save(user);
    }

    @Operation(summary = "Edita um usuário pelo ID")
    @PutMapping("/{id}")
    public User update(@Valid @RequestBody UserUpdateDTO user, @PathVariable Integer id) {
        return userService.update(user, id);
    }

    @Operation(summary = "Deleta um usuário pelo ID")
    @ApiResponse(responseCode = "204", description = "Sucesso sem corpo de resposta.", content = @Content)
    @ApiResponse(responseCode = "404", description = "Usuário não encontrado.", content = @Content)
    @ApiResponse(responseCode = "400", description = "Id inválido.", content = @Content)
    @ApiResponse(responseCode = "409", description = "Não foi possível excluir o usuário pois ele possui pedidos cadastrado.", content = @Content)
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer id, HttpServletRequest request) {
        userService.delete(id, request.getHeader("Authorization"));
    }

}
