package br.com.desafio2.ilab.usuarios.providers;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

public class OrderApiProvider {
    private final static String path = System.getenv("URL_API_ORDER");
    private static WebClient client = WebClient.create(path);

    public static Boolean canDeleteUser(Integer id, String token) {

        Mono<Boolean> result = client.get()
                .uri("/orders/user/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", token)
                .retrieve()
                .bodyToMono(Boolean.class);

        return result.block();
    }

}
